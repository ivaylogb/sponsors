// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Fan.h"
#include "ShooterGame/Classes/Combatants/Combatant.h"
#include "ShooterGame/Classes/Combatants/Alliance.h"
#include "DustinQuarter.generated.h"


/**
 * 
 */
UCLASS()
class SHOOTERGAME_API ADustinQuarter : public APawn
{
	GENERATED_UCLASS_BODY()
	
	public:
		UPROPERTY(EditAnywhere, Category = Behavior)
		TArray<AFan *> Fans;

		UPROPERTY(EditAnywhere, Category = Behavior)
		TArray<ACombatant *> Combatants;

		UPROPERTY(EditAnywhere, Category = Behavior)
		TArray<AAlliance *> Alliances;

		UFUNCTION(BlueprintCallable, Category = Behavior)
		void PopulateQuarter(TArray<AActor *> InActors);

		UFUNCTION(BlueprintCallable, Category = Behavior)
		TArray<ACombatant *> GetCombatants();
};

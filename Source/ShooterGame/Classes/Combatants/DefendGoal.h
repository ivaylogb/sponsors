// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "Goal.h"
#include "DefendGoal.generated.h"


UCLASS()
class UDefendGoal : public UGoal
{
	GENERATED_UCLASS_BODY()

    virtual GoalState GetGoalState() const override;
    
    virtual void OnGoalSuccess() override;
    
    virtual void OnGoalFailure() override;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    TArray<AActor *> PathPoints;
};
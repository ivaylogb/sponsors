// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "Path.generated.h"

UCLASS()
class APath : public AActor
{
	GENERATED_UCLASS_BODY()

public:
    UPROPERTY(EditAnywhere, Category = Behavior)
    TArray<ATargetPoint *> PathTargetPoints;

    UFUNCTION(BlueprintCallable, Category = Behavior)
    TArray<FVector> GetTargetPoints();
    
private:
};

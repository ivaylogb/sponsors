// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "AllianceBase.h"
#include "ShooterCharacter.h"
#include "Alliance.generated.h"

class ACombatant;
class UAllianceWarRoom;

const int NUM_ALLIANCE_ATTRIBUTE_TYPES = 7;
enum AllianceAttributeType { ALLIANCE_NONE = 0, ALLIANCE_BLOODLUST, ALLIANCE_JUSTICE, ALLIANCE_INDEPENDENCE, ALLIANCE_LOYALTY, ALLIANCE_EGO, ALLIANCE_SURVIVAL };

/*
* Raw util points should be in the range -100 to 100.
*/
typedef struct UtilPointVector { float vec[NUM_ALLIANCE_ATTRIBUTE_TYPES]; } UtilPointVector;

UCLASS()
class AAlliance : public AActor
{
	GENERATED_BODY()

public:
    
	void Destroyed();
    
    TArray<AShooterCharacter *> allianceMembers;
    
    UFUNCTION(BlueprintNativeEvent, Category = AllianceFunctions)
    void OnJoinedAlliance(AShooterCharacter *newMember);
    
    UFUNCTION(BlueprintNativeEvent, Category = AllianceFunctions)
    void OnLeftAlliance(AShooterCharacter *oldMember);
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    AAllianceBase * HomeBase;

	float wBloodlust();
	float wJustice();
	float wIndependence();
	float wEgo();
	float wSurvival();

	float wLoyalty();

	float GetJusticeScore();
	float GetBloodlustScore();
	float GetIndependenceScore();

    UFUNCTION(BlueprintCallable, Category = Behavior)
	FString name();

	FVector getCentroid();

	UFUNCTION(BlueprintCallable, Category = Behavior)
	FVector baseLocation();

	FVector locationOfBase;

	UFUNCTION(BlueprintCallable, Category = Behavior)
	void SetBaseLocation(FVector location);

	float utilsForRawPoints(UtilPointVector upv, FVector missionLocation);

	float numKillPoints;
	float numJustActionPoints;
	float numTotalActionPoints;

	DECLARE_EVENT_TwoParams(AAlliance, FOnMemberDeathEvent, AAlliance* /* alliance */, AShooterCharacter* /* killer */);
	FOnMemberDeathEvent OnMemberDeathEvent;

	DECLARE_EVENT_OneParam(AAlliance, FOnAllianceDisbandEvent, AAlliance* /* alliance */);
	FOnAllianceDisbandEvent OnAllianceDisbandEvent;

	static TArray<AAlliance*> getAllAlliances() { return AAlliance::allAlliances; }
    
	UFUNCTION(BlueprintCallable, Category = Behavior)
    static void regroupCombatants(TArray<ACombatant*> combatants, TArray<FVector> locations, float combatantCentroidRefusalDistance);

    FString adj;
    FString noun;

	UFUNCTION(BlueprintCallable, Category = Behavior)
	static bool FightingIsOver(TArray<ACombatant*> combatants);

protected:
	virtual void BeginPlay();
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);

private:
	void checkForNewName();

	void OnMemberDeath(AShooterCharacter* deadMember, AShooterCharacter* killer);
    
	static TArray<TArray<ACombatant *> > clusterShootersByPersonalityKMeans(TArray<ACombatant*> combatants, int32 k, float combatantCentroidRefusalDistance);

	typedef float(*WFunction)(ACombatant*);
	float calcWStat(WFunction wFn);

	UAllianceWarRoom* warRoom;

	AllianceAttributeType prevPrimary;
	AllianceAttributeType prevSecondary;

	static TArray<AAlliance*> allAlliances;
};

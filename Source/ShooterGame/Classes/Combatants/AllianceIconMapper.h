// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "Alliance.h"
#include "AllianceIconMapper.generated.h"

UCLASS()
class AAllianceIconMapper : public AActor
{
	GENERATED_UCLASS_BODY()

public:
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureNull;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureDemons;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureHellions;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureBerserkers;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureDestroyers;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureGhouls;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * texturePsychopaths;

    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureShields;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureKnights;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureProtectors;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureGuardians;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureHeroes;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureDefenders;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureRovers;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureRonin;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureRangers;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureHermits;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureOutcasts;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureRiders;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureStars;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureBraggarts;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureChampions;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureIdols;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureCelebrities;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureLeaders;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureSurvivalists;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureCockroaches;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureAbiders;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureAdaptors;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureWaters;
    UPROPERTY(EditAnywhere, Category = Behavior)
    UTexture2D * textureCowards;
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    UTexture2D * MapAllianceToTexture(AAlliance * alliance);
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    FColor MapAllianceToColor(AAlliance *alliance);
    
private:
};

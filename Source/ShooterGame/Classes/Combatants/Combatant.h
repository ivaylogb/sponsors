// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "Path.h"
#include "Alliance.h"
#include "Fan.h"
#include "Combatant.generated.h"

UENUM()
enum AUDIO_TYPES {
    WAR_CRY,
    ALERT_CRY,
    DEATH_CRY,
    DAMAGE_CRY
};

UCLASS()
class ACombatant : public AShooterCharacter
{
	GENERATED_UCLASS_BODY()
    
public:

    UPROPERTY(EditAnywhere, Category = Behavior)
    float W_BLOODLUST;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    float W_JUSTICE;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    float W_INDEPENDENCE;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    float W_LOYALTY;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    float W_EGO;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    float W_SURVIVAL;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    float KILL_POINTS_TICK_DROPOFF;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    float SIGHT_RANGE;
    
    float numKillPoints;

	UPROPERTY(EditAnywhere, Category = Behavior)
    float numJustActionPoints;

	UPROPERTY(EditAnywhere, Category = Behavior)
    float numTotalActionPoints;
    
    int32 numBloodlustFans;
    int32 numJusticeFans;
    int32 numLoneRangerFans;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    TArray<USoundCue *> warCries;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    TArray<USoundCue *> alertCries;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    TArray<USoundCue *> deathCries;
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    TArray<USoundCue *> damagedCries;
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void PlaySoundFX(AUDIO_TYPES audioType);
    
    void ResetSoundFX();
    
    bool isPlayingSoundFX;
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    bool HasSimilarPersonality(ACombatant* OtherCombatant, float MaxAngle);
    
    // get personality scores
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    float GetBloodlustScore();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    float GetJusticeScore();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    float GetIndependenceScore();
    
    // get weights cause UE4 is retarded
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    float GetWBloodLust();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    float GetWJustice();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    float GetWIndependence();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    float GetWEgo();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    float GetWSurvival();
    
    
    
    // add/remove fans
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void AddBloodlustFan();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void AddJusticeFan();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void AddLoneRangerFan();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void RemoveBloodlustFan();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void RemoveJusticeFan();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void RemoveLoneRangerFan();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    bool ShouldAttackEnemyFan(AFan *fan);
    
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    TArray<USoundCue *> GetWarCries();
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    AAlliance *alliance;
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void JoinAlliance(AAlliance *new_alliance);
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void LeaveAlliance();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    AAlliance *GetAlliance();
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    APath *path;
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    APath *GetPath();
    
    virtual bool IsEnemyFor(AController* TestPC) const override;
    
    virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;
	
	/** get max health */
	virtual int32 GetMaxHealth() const override;

	ACombatant* killedMe;

protected:

	virtual void OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser);
    
    virtual void OnKill(ACombatant *KilledCombatant);
    virtual void BeginPlay();
    virtual void Tick(float DeltaSeconds) override;
    
    void RandomizePersonality();

};

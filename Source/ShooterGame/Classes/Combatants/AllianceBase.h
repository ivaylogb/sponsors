// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "Path.h"
#include "AllianceBase.generated.h"

UCLASS()
class AAllianceBase : public AActor
{
	GENERATED_UCLASS_BODY()

public:
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    TArray<APath *> DefendPaths;
    
private:
};

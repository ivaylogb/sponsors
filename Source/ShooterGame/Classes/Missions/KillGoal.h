// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Combatants/Alliance.h"
#include "CombatantGoal.h"
#include "../Combatants/Alliance.h"
#include "KillGoal.generated.h"

#define BLOODLUST_PER_TARGET_MEMBER 10
#define SURVIVAL_LOSS_PER_TARGET_MEMBER 5
#define ALLIANCE_SURVIVAL_JUSTICE_DIFF_WEIGHT 0.25
#define ALLIANCE_SURVIVAL_BLOODLUST_WEIGHT 0.75
#define ALLIANCE_SURVIVAL_WEIGHT_NUM (ALLIANCE_SURVIVAL_JUSTICE_DIFF_WEIGHT + ALLIANCE_SURVIVAL_BLOODLUST_WEIGHT)
#define ALLIANCE_SURVIVAL_DISTANCE_FUNC(dist) (50000.0 / (50000.0 + dist))

/**
 * TODO: Catch event that allows kills attained to be incremented.
 */
UCLASS(CustomConstructor)
class SHOOTERGAME_API UKillGoal : public UCombatantGoal
{
	GENERATED_BODY()

public:
	UKillGoal(const FObjectInitializer& ObjectInitializer);

	AAlliance* myAlliance;
	void optimize();

	AAlliance* target() { return targetAlliance; };

	int getNumKills() { return numKillsAttained; };

protected:
	virtual void optimizeForAlliance(AAlliance* alliance);
	virtual void recomputeUtilPoints();

private:
	void addKill(AAlliance* target, AShooterCharacter* killer);
	void targetDisbanded(AAlliance* target);

	void setTargetAlliance(AAlliance* target);

	AAlliance* targetAlliance;
	int numKillsAttained;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Combatants/Alliance.h"
#include "AllianceWarRoom.generated.h"

/**
 * 
 */
UCLASS(CustomConstructor)
class SHOOTERGAME_API UAllianceWarRoom : public UObject
{
	GENERATED_BODY()
	
public:
	UAllianceWarRoom(const FObjectInitializer& ObjectInitializer);

	AAlliance* alliance;

	float utilsForRawPoints(UtilPointVector upv, FVector missionLocation);
	float rawUtilPoints(AllianceAttributeType pointType) { return computedUtilPoints.vec[pointType]; };

private:
	
	UtilPointVector computedUtilPoints;
	virtual void recomputeUtilPoints();
};

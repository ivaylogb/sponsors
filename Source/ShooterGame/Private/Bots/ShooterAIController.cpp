// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

AShooterAIController::AShooterAIController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	BlackboardComp = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackBoardComp"));
 	
	BrainComponent = BehaviorComp = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));	

	bWantsPlayerState = true;
}

void AShooterAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	AShooterBot* Bot = Cast<AShooterBot>(InPawn);

	// start behavior
	if (Bot && Bot->BotBehavior)
	{
		BlackboardComp->InitializeBlackboard(Bot->BotBehavior->BlackboardAsset);

		EnemyKeyID = BlackboardComp->GetKeyID("Enemy");
		NeedAmmoKeyID = BlackboardComp->GetKeyID("NeedAmmo");
        HasPathKeyID = BlackboardComp->GetKeyID("HasPath");
        
        DidHearKeyID = BlackboardComp->GetKeyID("DidHearRecently");
        HeardPositionKeyID = BlackboardComp->GetKeyID("HeardPosition");
		CombatQuarterID = BlackboardComp->GetKeyID("CombatQuarter");

		BehaviorComp->StartTree(*(Bot->BotBehavior));
	}
}

void AShooterAIController::BeginInactiveState()
{
	Super::BeginInactiveState();

	AGameState* GameState = GetWorld()->GameState;

	const float MinRespawnDelay = (GameState && GameState->GameModeClass) ? GetDefault<AGameMode>(GameState->GameModeClass)->MinRespawnDelay : 1.0f;

	//GetWorldTimerManager().SetTimer(this, &AShooterAIController::Respawn, MinRespawnDelay);
}

void AShooterAIController::Respawn()
{
	//GetWorld()->GetAuthGameMode()->RestartPlayer(this);
}


TArray<AShooterCharacter *> AShooterAIController::GetEnemies()
{
    TArray<AShooterCharacter *> enemiesInMap;
    for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
    {
        ACombatant* TestPawn = Cast<ACombatant>(*It);
        if (TestPawn && TestPawn->IsAlive() && TestPawn->IsEnemyFor(this))
        {
            enemiesInMap.Add(Cast<AShooterCharacter>(TestPawn));
        }
    }
    return enemiesInMap;
}

void AShooterAIController::FindClosestEnemy(bool AttackFans)
{
	APawn* MyBot = GetPawn();
	if (MyBot == NULL)
	{
		return;
	}

	const FVector MyLoc = MyBot->GetActorLocation();
	float BestDistSq = MAX_FLT;
	AShooterCharacter* BestPawnShooter = NULL;

	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		if (!AttackFans){


			ACombatant* TestPawn = Cast<ACombatant>(*It);
			if (TestPawn && TestPawn->IsAlive())
			{
				AShooterAIController* Controller = Cast<AShooterAIController>(TestPawn->GetController());
				if (Controller->GetCombatQuarter() == GetCombatQuarter() && TestPawn->IsEnemyFor(this))
				{
					const float DistSq = (TestPawn->GetActorLocation() - MyLoc).SizeSquared();
					if (DistSq < BestDistSq)
					{
						BestDistSq = DistSq;
						BestPawnShooter = Cast<AShooterCharacter>(TestPawn);
					}
				}
			}
		}
		if (AttackFans)
		{
			AFan* FanPawn = Cast<AFan>(*It);
			if (FanPawn && FanPawn->IsAlive())
			{
				AFanAIController* FanController = Cast<AFanAIController>(FanPawn->GetController());
				if (FanController->GetCombatQuarter() == GetCombatQuarter())
				{
					bool enemyFan = true;
					if (FanController->GetIdol()){
						ACombatant* idolCombatant = Cast<ACombatant>(FanController->GetIdol());
						if (idolCombatant) enemyFan = idolCombatant->IsEnemyFor(this);
					}
					ACombatant *MyCombatant = Cast<ACombatant>(MyBot);
					if (enemyFan && MyCombatant && MyCombatant->ShouldAttackEnemyFan(FanPawn))
					{
						const float DistSq = (FanPawn->GetActorLocation() - MyLoc).SizeSquared();
						if (DistSq < BestDistSq)
						{
							//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "All fans go to hell.");
							BestDistSq = DistSq;
							BestPawnShooter = Cast<AShooterCharacter>(FanPawn);
						}
					}
				}
			}
		}
		
	}

	if (BestPawnShooter)
	{
        SetEnemy(BestPawnShooter);
	}
}

void AShooterAIController::SetLastHeardSoundLocation(FVector loc)
{
    if (BlackboardComp)
    {
        BlackboardComp->SetValueAsBool(DidHearKeyID, true);
        BlackboardComp->SetValueAsVector(HeardPositionKeyID, loc);
        const float MinHearingResetDelay = 10.0;
        GetWorldTimerManager().ClearTimer(this, &AShooterAIController::ResetHeardSound);
        GetWorldTimerManager().SetTimer(this, &AShooterAIController::ResetHeardSound, MinHearingResetDelay);
        ACombatant *Combatant = Cast<ACombatant>(GetPawn());
        if (Combatant) {
            Combatant->PlaySoundFX(AUDIO_TYPES::ALERT_CRY);
        }
    }
}

void AShooterAIController::SetCombatQuarter(class APawn* InPawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(CombatQuarterID, InPawn);
		SetFocus(InPawn);
	}
}

class APawn* AShooterAIController::GetCombatQuarter()
{
	if (BlackboardComp)
	{
		return Cast<APawn>(BlackboardComp->GetValueAsObject(CombatQuarterID));
	}
	return NULL;
}

void AShooterAIController::ResetHeardSound()
{
    BlackboardComp->SetValueAsBool(DidHearKeyID, false);
}

bool AShooterAIController::HasPathToPatrol()
{
    ACombatant *combatant = Cast<ACombatant>(GetPawn());
    return (combatant && combatant->GetPath());
}

bool AShooterAIController::FindClosestEnemyWithLOS(AShooterCharacter* ExcludeEnemy, bool AttackFans)
{
	bool bGotEnemy = false;
	ACombatant* MyBot = Cast<ACombatant>(GetPawn());
	if (MyBot != NULL)
	{
		const FVector MyLoc = MyBot->GetActorLocation();
		float BestNetFactor = -MAX_FLT;
		AShooterCharacter* BestPawnShooter = NULL;

		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
		{
			if (!AttackFans)
			{
				ACombatant* TestPawn = Cast<ACombatant>(*It);
				if (TestPawn && TestPawn != ExcludeEnemy && TestPawn->IsAlive())
				{
					AShooterAIController* Controller = Cast<AShooterAIController>(TestPawn->GetController());
					if (Controller->GetCombatQuarter() == GetCombatQuarter() && TestPawn->IsEnemyFor(this)){
						if (HasWeaponLOSToEnemy(TestPawn, true) == true)
						{
							float dist = (TestPawn->GetActorLocation() - MyLoc).Size();
							if (dist < MyBot->SIGHT_RANGE) {
								const float parameterFactor = (100 * 100);
								float distFactor = (1.0 - dist / MyBot->SIGHT_RANGE);
								//Getting compilation error for fmin, not resloving types properly
								float healthFactor = fmin(1.0, 1.0 * MyBot->Health / TestPawn->Health);
								float bloodlustFactor = -MyBot->W_BLOODLUST * TestPawn->W_BLOODLUST / parameterFactor;
								float independenceFactor = -MyBot->W_INDEPENDENCE * TestPawn->W_INDEPENDENCE / parameterFactor;
								float justiceFactor = -MyBot->W_JUSTICE * TestPawn->W_JUSTICE / parameterFactor;
								float coopFactor = -MyBot->W_LOYALTY * TestPawn->W_LOYALTY / parameterFactor;
								float survivalFactor = -MyBot->W_SURVIVAL * TestPawn->W_SURVIVAL / parameterFactor;
								float netFactor = distFactor + healthFactor + bloodlustFactor + independenceFactor +
									justiceFactor + coopFactor + survivalFactor;
								if (netFactor > BestNetFactor) {
									BestNetFactor = netFactor;
									BestPawnShooter = Cast<AShooterCharacter>(TestPawn);
								}
							}
						}
					}
				}
			}
			if (AttackFans)
			{
				AFan* FanPawn = Cast<AFan>(*It);

				if (FanPawn && FanPawn != ExcludeEnemy && FanPawn->IsAlive() /*not your fan*/)
				{
					AFanAIController* FanController = Cast<AFanAIController>(FanPawn->GetController());
					if (FanController->GetCombatQuarter() == GetCombatQuarter()){
						bool enemyFan = true;
						if (FanController->GetIdol()){
							ACombatant* idolCombatant = Cast<ACombatant>(FanController->GetIdol());
							if (idolCombatant) enemyFan = idolCombatant->IsEnemyFor(this);
						}
						if (enemyFan && MyBot->ShouldAttackEnemyFan(FanPawn))
						{
							if (HasWeaponLOSToEnemy(FanPawn, true) == true)
							{
								float dist = (FanPawn->GetActorLocation() - MyLoc).Size();
								if (dist < MyBot->SIGHT_RANGE) {
									const float parameterFactor = (100 * 100);
									float distFactor = (1.0 - dist / MyBot->SIGHT_RANGE);
									//Getting compilation error for fmin, not resloving types properly
									float healthFactor = fmin(1.0, 1.0 * MyBot->Health / FanPawn->Health);

									float netFactor = healthFactor;
									if (netFactor > BestNetFactor) {
										//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "All fans go to hell (LOS).");
										BestNetFactor = netFactor;
										BestPawnShooter = Cast<AShooterCharacter>(FanPawn);
									}
								}
							}
						}
					}
				}
			}
		}
		if (BestPawnShooter)
		{
            SetEnemy(BestPawnShooter);
			bGotEnemy = true;
		}
	}
	return bGotEnemy;
}

bool AShooterAIController::HasWeaponLOSToEnemy(AActor* InEnemyActor, const bool bAnyEnemy) const
{
	static FName LosTag = FName(TEXT("AIWeaponLosTrace"));
	
	AShooterBot* MyBot = Cast<AShooterBot>(GetPawn());

	bool bHasLOS = false;
	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(LosTag, true, GetPawn());
	TraceParams.bTraceAsyncScene = true;

	TraceParams.bReturnPhysicalMaterial = true;	
	FVector StartLocation = MyBot->GetActorLocation();	
	StartLocation.Z += GetPawn()->BaseEyeHeight; //look from eyes
	
	FHitResult Hit(ForceInit);
	const FVector EndLocation = InEnemyActor->GetActorLocation();
	GetWorld()->LineTraceSingle(Hit, StartLocation, EndLocation, COLLISION_WEAPON, TraceParams);
	if (Hit.bBlockingHit == true)
	{
		// Theres a blocking hit - check if its our enemy actor
		AActor* HitActor = Hit.GetActor();
		if (Hit.GetActor() != NULL)
		{
			if (HitActor == InEnemyActor)
			{
				bHasLOS = true;
			}
			else if (bAnyEnemy == true)
			{
				// Its not our actor, maybe its still an enemy ?
				ACharacter* HitChar = Cast<ACharacter>(HitActor);
				if (HitChar != NULL)
				{
					AShooterPlayerState* HitPlayerState = Cast<AShooterPlayerState>(HitChar->PlayerState);
					AShooterPlayerState* MyPlayerState = Cast<AShooterPlayerState>(PlayerState);
					if ((HitPlayerState != NULL) && (MyPlayerState != NULL))
					{
						if (HitPlayerState->GetTeamNum() != MyPlayerState->GetTeamNum())
						{
							bHasLOS = true;
						}
					}
				}
			}
		}
	}

	

	return bHasLOS;
}

void AShooterAIController::ShootEnemy()
{
	ACombatant* MyBot = Cast<ACombatant>(GetPawn());
	AShooterWeapon* MyWeapon = MyBot ? MyBot->GetWeapon() : NULL;
	if (MyWeapon == NULL)
	{
		return;
	}

	bool bCanShoot = false;
	AShooterCharacter* Enemy = GetEnemy();
    
    if(Enemy && (!Enemy->IsAlive() || (MyBot->GetActorLocation()-Enemy->GetActorLocation()).Size() > MyBot->SIGHT_RANGE)) {
        SetEnemy(nullptr);
    }
    
	if ( Enemy && ( Enemy->IsAlive() )&& (MyWeapon->GetCurrentAmmo() > 0) && ( MyWeapon->CanFire() == true ) )
	{
		if (LineOfSightTo(Enemy, MyBot->GetActorLocation()))
		{
			bCanShoot = true;
		}
	}

	if (bCanShoot)
	{
		MyBot->StartWeaponFire();
	}
	else
	{
		MyBot->StopWeaponFire();
	}
}

void AShooterAIController::CheckAmmo(const class AShooterWeapon* CurrentWeapon)
{
	if (CurrentWeapon && BlackboardComp)
	{
		const int32 Ammo = CurrentWeapon->GetCurrentAmmo();
		const int32 MaxAmmo = CurrentWeapon->GetMaxAmmo();
		const float Ratio = (float) Ammo / (float) MaxAmmo;

		BlackboardComp->SetValueAsBool(NeedAmmoKeyID, (Ratio <= 0.1f));
	}
}

void AShooterAIController::SetEnemy(class APawn* InPawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(EnemyKeyID, InPawn);
		SetFocus(InPawn);
	}
    
    // play a war cry
    ACombatant *Combatant = Cast<ACombatant>(GetPawn());
    if (Combatant) {
        Combatant->PlaySoundFX(AUDIO_TYPES::WAR_CRY);
    }
}

class AShooterCharacter* AShooterAIController::GetEnemy() const
{
	if (BlackboardComp)
	{
		return Cast<AShooterCharacter>(BlackboardComp->GetValueAsObject(EnemyKeyID));
	}

	return NULL;
}


void AShooterAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
{
	// Look toward focus
	FVector FocalPoint = GetFocalPoint();
	if( !FocalPoint.IsZero() && GetPawn())
	{
		FVector Direction = FocalPoint - GetPawn()->GetActorLocation();
		FRotator NewControlRotation = Direction.Rotation();
		
		NewControlRotation.Yaw = FRotator::ClampAxis(NewControlRotation.Yaw);

		SetControlRotation(NewControlRotation);

		APawn* const P = GetPawn();
		if (P && bUpdatePawn)
		{
			P->FaceRotation(NewControlRotation, DeltaTime);
		}
		
	}
}

void AShooterAIController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	// Stop the behaviour tree/logic
	BehaviorComp->StopTree();

	// Stop any movement we already have
	StopMovement();

	// Cancel the repsawn timer
	GetWorldTimerManager().ClearTimer(this, &AShooterAIController::Respawn);

	// Clear any enemy
	SetEnemy(NULL);

	// Finally stop firing
	AShooterBot* MyBot = Cast<AShooterBot>(GetPawn());
	AShooterWeapon* MyWeapon = MyBot ? MyBot->GetWeapon() : NULL;
	if (MyWeapon == NULL)
	{
		return;
	}
	MyBot->StopWeaponFire();	
}


// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "../../Classes/Missions/PatrolGoal.h"

UPatrolGoal::UPatrolGoal(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	myAlliance = NULL;
	patrolPosition = FVector(0, 0, 0);
}

void UPatrolGoal::optimize() {
	if (myAlliance) optimizeForAlliance(myAlliance);
}

void UPatrolGoal::optimizeForAlliance(AAlliance* alliance) {
	// TODO: Implement.
}

void UPatrolGoal::recomputeUtilPoints() {
	// TODO: Implement.
}
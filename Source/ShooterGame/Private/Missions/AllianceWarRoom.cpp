// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "../../Classes/Missions/AllianceWarRoom.h"

UAllianceWarRoom::UAllianceWarRoom(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	for (int i = ALLIANCE_NONE; i < NUM_ALLIANCE_ATTRIBUTE_TYPES; i++) { computedUtilPoints.vec[i] = 0.0; }
}

float UAllianceWarRoom::utilsForRawPoints(UtilPointVector upv, FVector missionLocation) {
	// TODO: Implement.
	return 1.0;
}


void UAllianceWarRoom::recomputeUtilPoints() {
	// TODO: Implement.
}
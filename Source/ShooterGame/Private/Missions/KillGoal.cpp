// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "../../Classes/Missions/KillGoal.h"
#include "../../Classes/Combatants/Combatant.h"

UKillGoal::UKillGoal(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	myAlliance = NULL;
}

void UKillGoal::optimize() { 
	if (myAlliance) optimizeForAlliance(myAlliance);
}

void UKillGoal::optimizeForAlliance(AAlliance* alliance) {
	// Iterate over possible targets.
	AAlliance* bestTarget = NULL;
	float bestUtils = 0.0;
	for (auto possibleTarget : AAlliance::getAllAlliances()) {
		if (possibleTarget == alliance) continue;
		this->targetAlliance = possibleTarget;
		this->recomputeUtilPoints();
		float utils = this->myAlliance->utilsForRawPoints(computedUtilPoints, targetAlliance->baseLocation());
		if (utils > bestUtils) {
			bestUtils = utils;
			bestTarget = possibleTarget;
		}
	}

	this->targetAlliance = NULL; // We should set target with setter function, so we reset it here.
	if (bestTarget) setTargetAlliance(bestTarget);
}

void UKillGoal::recomputeUtilPoints() {
	float justicePoints = 0.0;
	justicePoints -= targetAlliance->GetJusticeScore();
	// TODO: Normalize?
	computedUtilPoints.vec[ALLIANCE_JUSTICE] = justicePoints;

	float bloodlustPoints = 0.0;
	bloodlustPoints += BLOODLUST_PER_TARGET_MEMBER * targetAlliance->allianceMembers.Num();
	bloodlustPoints = (bloodlustPoints > 100) ? 100 : bloodlustPoints;
	computedUtilPoints.vec[ALLIANCE_BLOODLUST] = bloodlustPoints;

	float egoPoints = 0.0;
	// TODO: Implement.
	computedUtilPoints.vec[ALLIANCE_EGO] = egoPoints;

	float indPoints = 0.0;
	computedUtilPoints.vec[ALLIANCE_INDEPENDENCE] = indPoints;

	float survPoints = 0.0;
	// Model how killing target alliance affects our alliance survival.
	float allSurvPoints = 0.0;
	// 1) If target is just, and we are just, or unjust and we are unjust, we have little to fear,
	// so no need to attack to survive. (Divide by 100 to get a range of -100 to 100)
	allSurvPoints -= (targetAlliance->GetJusticeScore() * myAlliance->GetJusticeScore() / 100) * ALLIANCE_SURVIVAL_JUSTICE_DIFF_WEIGHT;
	// 2) If the target is bloodlusty, we have more to fear.
	allSurvPoints += (targetAlliance->GetBloodlustScore()) * ALLIANCE_SURVIVAL_BLOODLUST_WEIGHT;
	// TODO: 2.5) Include existing hostility?
	// 3) If the target is distant, it is less of a threat.
	float dist = (myAlliance->baseLocation() - targetAlliance->baseLocation()).Size();
	allSurvPoints *= ALLIANCE_SURVIVAL_DISTANCE_FUNC(dist);

	// Model how fighting target affects individual survival.
	float indSurvPoints = 0.0;
	// If there are a lot of bad guys, lower chance of survival.
	indSurvPoints -= SURVIVAL_LOSS_PER_TARGET_MEMBER * targetAlliance->allianceMembers.Num();
	indSurvPoints = (indSurvPoints > 100) ? 100 : indSurvPoints;

	// Weight group and individual survival by alliance individualism.
	float indFrac = (myAlliance->wIndependence() + 100) / 200.0;
	survPoints = allSurvPoints * (1 - indFrac) + indSurvPoints * (indFrac);
	computedUtilPoints.vec[ALLIANCE_SURVIVAL] = survPoints;

	//GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("allSurvPoints: " + FString::SanitizeFloat(allSurvPoints)));
	//GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("indSurvPoints: " + FString::SanitizeFloat(indSurvPoints)));
}

void UKillGoal::setTargetAlliance(AAlliance* target) {
	if (this->targetAlliance) {
		this->targetAlliance->OnMemberDeathEvent.RemoveUObject(this, &UKillGoal::addKill);
	}
	this->targetAlliance = target;
	this->targetAlliance->OnMemberDeathEvent.AddUObject(this, &UKillGoal::addKill);

	this->targetAlliance->OnAllianceDisbandEvent.AddUObject(this, &UKillGoal::targetDisbanded);
}

void UKillGoal::addKill(AAlliance* target, AShooterCharacter* killer) {
	ACombatant* com = Cast<ACombatant>(killer);
	if (com && com->alliance == this->myAlliance) numKillsAttained++;
}

void UKillGoal::targetDisbanded(AAlliance* target) {
	OnGoalSuccess.Broadcast(this);
}
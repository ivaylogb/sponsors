// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"

AAllianceIconMapper::AAllianceIconMapper(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

const FString BL_ADJ1[7] = {
    "Red",
    "Bloody",
    "Deadly",
    "Wrathful",
    "Satanic",
    "Gruesome",
    "Chaotic"
};

const FString JUST_ADJ1[7] = {
    "Blue",
    "Ardent",
    "Fervent",
    "Just",
    "Righteous",
    "Godly",
    "Merciful"
};

const FString IND_ADJ1[7] = {
    "Black",
    "Lone",
    "Mysterious",
    "Shady",
    "Silent",
    "Distant",
    "Reserved"
};

const FString EGO_ADJ1[7] = {
    "Green",
    "Boastful",
    "Loud",
    "Flashy",
    "Famous",
    "Charismatic",
    "Charming"
};

const FString SURV_ADJ1[7] = {
    "Yellow",
    "Skittish",
    "Lasting",
    "Prudent",
    "Undying",
    "Elusive",
    "Slippery"
};


FColor AAllianceIconMapper::MapAllianceToColor(AAlliance * alliance)
{
    FColor col = FColor(255,255,255,255);
    if(alliance == nullptr)
        return col;
    
    for(int i = 0; i < 7; i++) {
        if(alliance->adj.Equals(BL_ADJ1[i])) {
            return FColor(255,0,0,255);
        }
        if(alliance->adj.Equals(JUST_ADJ1[i])) {
            return FColor(0,127,255,255);
        }
        if(alliance->adj.Equals(IND_ADJ1[i])) {
            return FColor(255,255,255,255);
        }
        if(alliance->adj.Equals(EGO_ADJ1[i])) {
            return FColor(0,255,0,255);
        }
        if(alliance->adj.Equals(SURV_ADJ1[i])) {
            return FColor(255,255,0,255);
        }
    }
    return col;
    
}

UTexture2D * AAllianceIconMapper::MapAllianceToTexture(AAlliance * alliance)
{
    if(alliance == nullptr) {
        return textureNull;
    }
    else if(alliance->noun.Equals("Demons")) {
        return textureDemons;
    }
    else if(alliance->noun.Equals("Hellions")) {
        return textureHellions;
    }
    else if(alliance->noun.Equals("Berserkers")) {
        return textureBerserkers;
    }
    
    else if(alliance->noun.Equals("Destroyers")) {
        return textureDestroyers;
    }
    
    else if(alliance->noun.Equals("Ghouls")) {
        return textureGhouls;
    }
    
    else if(alliance->noun.Equals("Psychopaths")) {
        return texturePsychopaths;
    }
    
    else if(alliance->noun.Equals("Shields")) {
        return textureShields;
    }
    
    else if(alliance->noun.Equals("Knights")) {
        return textureKnights;
    }
    
    else if(alliance->noun.Equals("Protectors")) {
        return textureProtectors;
    }
    
    else if(alliance->noun.Equals("Guardians")) {
        return textureGuardians;
    }
    
    else if(alliance->noun.Equals("Heroes")) {
        return textureHeroes;
    }
    
    else if(alliance->noun.Equals("Defenders")) {
        return textureDefenders;
    }
    
    else if(alliance->noun.Equals("Rovers")) {
        return textureRovers;
    }
    
    else if(alliance->noun.Equals("Ronin")) {
        return textureRonin;
    }
    
    else if(alliance->noun.Equals("Rangers")) {
        return textureRangers;
    }
    
    else if(alliance->noun.Equals("Hermits")) {
        return textureHermits;
    }
    
    else if(alliance->noun.Equals("Outcasts")) {
        return textureOutcasts;
    }
    
    else if(alliance->noun.Equals("Riders")) {
        return textureRiders;
    }
    
    else if(alliance->noun.Equals("Stars")) {
        return textureStars;
    }
    
    else if(alliance->noun.Equals("Braggarts")) {
        return textureBraggarts;
    }
    
    else if(alliance->noun.Equals("Champions")) {
        return textureChampions;
    }
    
    else if(alliance->noun.Equals("Idols")) {
        return textureIdols;
    }
    
    else if(alliance->noun.Equals("Celebrities")) {
        return textureCelebrities;
    }
    
    else if(alliance->noun.Equals("Leaders")) {
        return textureLeaders;
    }
    
    else if(alliance->noun.Equals("Survivalists")) {
        return textureSurvivalists;
    }
    
    else if(alliance->noun.Equals("Cockroaches")) {
        return textureCockroaches;
    }
    
    else if(alliance->noun.Equals("Abiders")) {
        return textureAbiders;
    }
    
    else if(alliance->noun.Equals("Adaptors")) {
        return textureAdaptors;
    }
    
    else if(alliance->noun.Equals("Waters")) {
        return textureWaters;
    }
    
    else if(alliance->noun.Equals("Cowards")) {
        return textureCowards;
    }
    return textureNull;
    /*
    switch(alliance->noun) {
        case "Demons":
            return textureDemons;
            
        case "Hellions":
            return textureHellions;
            
        case "Berserkers":
            return textureBerserkers;
            
        case "Destroyers":
            return textureDestroyers;
            
        case "Ghouls":
            return textureGhouls;
            
        case "Psychopaths":
            return texturePsychopaths;
            
        case "Shields":
            return textureShields;
            
        case "Knights":
            return textureKnights;
            
        case "Protectors":
            return textureProtectors;
            
        case "Guardians":
            return textureGuardians;
            
        case "Heroes":
            return textureHeroes;
            
        case "Defenders":
            return textureDefenders;
            
        case "Rovers":
            return textureRovers;
            
        case "Ronin":
            return textureRonin;
            
        case "Rangers":
            return textureRangers;
            
        case "Hermits":
            return textureHermits;
            
        case "Outcasts":
            return textureOutcasts;
            
        case "Riders":
            return textureRiders;
            
        case "Stars":
            return textureStars;
            
        case "Braggarts":
            return textureBraggarts;
            
        case "Champions":
            return textureChampions;
            
        case "Idols":
            return textureIdols;
            
        case "Celebrities":
            return textureCelebrities;
            
        case "Leaders":
            return textureLeaders;
            
        case "Survivalists":
            return textureSurvivalists;
            
        case "Cockroaches":
            return textureCockroaches;
            
        case "Abiders":
            return textureAbiders;
            
        case "Adaptors":
            return textureAdaptors;
            
        case "Waters":
            return textureWaters;
            
        case "Cowards":
            return textureCowards;
    }
    */
}
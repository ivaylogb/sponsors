// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#include "ShooterGame.h"

UDefendGoal::UDefendGoal(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

GoalState UDefendGoal::GetGoalState() const
{
    return GoalState::IN_PROGRESS;
}

void UDefendGoal::OnGoalSuccess()
{
    
}

void UDefendGoal::OnGoalFailure()
{
    
}
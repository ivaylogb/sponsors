// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Engine.h"

ACombatant::ACombatant(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	killedMe = nullptr;
    W_BLOODLUST = 0;
    W_JUSTICE = 0;
    W_INDEPENDENCE = 0;
    W_LOYALTY = 0;
    W_EGO = 0;
    W_SURVIVAL = 0;
    
    SIGHT_RANGE = 6000;
    
    // -1 kill per minute
    KILL_POINTS_TICK_DROPOFF = 0.0166667;
    
    numKillPoints = 0;
    numJustActionPoints = 1;
    numTotalActionPoints = 0;
    //alliance = nullptr;
    
    numBloodlustFans = 0;
    numJusticeFans = 0;
    numLoneRangerFans = 0;
    isPlayingSoundFX = false;
}

float ACombatant::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
                             class AController* EventInstigator, class AActor* DamageCauser)
{
    
    const FVector MyLoc = this->GetActorLocation();
    const float HearingDist = 12000;
    for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
    {
        ACombatant* TestPawn = Cast<ACombatant>(*It);
        if (TestPawn && TestPawn->IsAlive())
        {
            const float DistSq = (TestPawn->GetActorLocation() - MyLoc).Size();
            if (DistSq < HearingDist)
            {
                AShooterAIController *aiCon = Cast<AShooterAIController>(TestPawn->GetController());
                if(aiCon)
                {
                    aiCon->SetLastHeardSoundLocation(MyLoc);
                }
            }
        }
    }
    
    this->PlaySoundFX(AUDIO_TYPES::DAMAGE_CRY);
    
    return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

int32 ACombatant::GetMaxHealth() const {
	return Super::GetMaxHealth() + numJusticeFans*10;
}

bool ACombatant::HasSimilarPersonality(ACombatant* OtherCombatant, float Threshold)
{
    if(!OtherCombatant)
        return false;
    
    //Compute dot product of personality vectors
    float dp = OtherCombatant->W_BLOODLUST * W_BLOODLUST +
    OtherCombatant->W_INDEPENDENCE * W_INDEPENDENCE +
    OtherCombatant->W_JUSTICE * W_JUSTICE +
    OtherCombatant->W_LOYALTY * W_LOYALTY +
    OtherCombatant->W_EGO * W_EGO +
    OtherCombatant->W_SURVIVAL * W_SURVIVAL;
    return dp > Threshold;
}

float ACombatant::GetBloodlustScore() {
    return numKillPoints;
}

float ACombatant::GetJusticeScore() {
    //return numJustActionPoints / (1.0 + numTotalActionPoints);
	return numJustActionPoints;
}

float ACombatant::GetIndependenceScore() {
    if (!alliance) {
        return 0;
    }
    return alliance->allianceMembers.Num();
}


float ACombatant::GetWBloodLust() {
    return W_BLOODLUST;
}

float ACombatant::GetWJustice() {
    return W_JUSTICE;
}

float ACombatant::GetWIndependence() {
    return W_INDEPENDENCE;
}

float ACombatant::GetWEgo() {
    return W_EGO;
}

float ACombatant::GetWSurvival() {
    return W_SURVIVAL;
}


void ACombatant::AddBloodlustFan() {
    numBloodlustFans++;
}

void ACombatant::AddJusticeFan() {
    numJusticeFans++;
}

void ACombatant::AddLoneRangerFan() {
    numLoneRangerFans++;
}

void ACombatant::RemoveBloodlustFan() {
    numBloodlustFans--;
}

void ACombatant::RemoveJusticeFan() {
    numJusticeFans--;
}

void ACombatant::RemoveLoneRangerFan() {
    numLoneRangerFans--;
}

bool ACombatant::ShouldAttackEnemyFan(AFan *fan) {
	//if (true) return true;
    // bloodlusty players kill EVERYONE
    if (W_BLOODLUST > 50) {
        return true;
    }
    
    // not bloodlusty characters kill no one
    if (W_BLOODLUST < 30) {
        return false;
    }
    
    // lone rangers don't draw attention to themselves
    if (W_INDEPENDENCE > 90) {
        return false;
    }
    
    // just players will not kill just fans
    if (W_JUSTICE > 50) {
        if (fan->FAN_TYPE == EFAN_TYPE::EF_Just) {
            int32 justiceDiscrepancy = W_JUSTICE - fan->W_DESIRED_FEATURE;
            if (justiceDiscrepancy < 50) {
                // save the truly just
                return false;
            } else {
                // kill the posers
                return true;
            }
        } else {
            // just players kill bloodlusty fans
            return true;
        }
    } else {
        // unjust players MURDER
        return true;
    }
    return true;
}

TArray<USoundCue *> ACombatant::GetWarCries() {
    return warCries;
}

void ACombatant::JoinAlliance(AAlliance *new_alliance)
{
	AShooterCharacter* SelfAsShooter = Cast<AShooterCharacter>(this);

    //Only join an alliance once
	if (new_alliance == this->alliance){
		new_alliance->OnJoinedAlliance(SelfAsShooter);
		return;
	}
    
    LeaveAlliance(); //Leave the old alliance if necessary
   
    //Trigger the OnJoinedAlliance Event
    new_alliance->OnJoinedAlliance(SelfAsShooter);
    this->alliance = new_alliance;
}

void ACombatant::LeaveAlliance()
{
    if(alliance == nullptr)
        return;
    
    AShooterCharacter* SelfAsShooter = Cast<AShooterCharacter>(this);
    //Trigger the OnLeftAlliance Event
    alliance->OnLeftAlliance(SelfAsShooter);
    alliance = nullptr;
}

AAlliance * ACombatant::GetAlliance()
{
    return alliance;
}

APath * ACombatant::GetPath()
{
    return path;
}


void ACombatant::PlaySoundFX(AUDIO_TYPES audioType)
{
    bool isPlayOnceAudio = false;
    isPlayOnceAudio = (audioType == AUDIO_TYPES::WAR_CRY);
    isPlayOnceAudio |= (audioType == AUDIO_TYPES::ALERT_CRY);
    if(isPlayingSoundFX && isPlayOnceAudio)
        return;
    
    TArray<USoundCue *> *audioArray = nullptr;
    switch(audioType)
    {
        case AUDIO_TYPES::ALERT_CRY:
            audioArray = &this->alertCries;
            break;
        case AUDIO_TYPES::DAMAGE_CRY:
            audioArray = &this->damagedCries;
            break;
        case AUDIO_TYPES::DEATH_CRY:
            audioArray = &this->deathCries;
            break;
        case AUDIO_TYPES::WAR_CRY:
            audioArray = &this->warCries;
            break;
    }
    
    const float probPlaySound = 0.2;
    float randNum = (float)rand()/INT_MAX;
    float duration = 0;
    if(audioArray->Num() > 0 && randNum < probPlaySound)
    {
        int randIdx = rand() % audioArray->Num();
        USoundCue *cue = (*audioArray)[randIdx];
        this->PlaySoundOnActor(cue);
        isPlayingSoundFX = true;
        duration += cue->Duration;
    }
    
    const float MinSecsTilNextSound = 3.0; //In seconds
    const float MaxNumSecondsWait = 10.0;
    float RandOffset =  (float)rand()/INT_MAX;
    RandOffset *= MaxNumSecondsWait;
    RandOffset += MinSecsTilNextSound;
    if(isPlayOnceAudio)
    {
        GetWorldTimerManager().ClearTimer(this, &ACombatant::ResetSoundFX);
        GetWorldTimerManager().SetTimer(this, &ACombatant::ResetSoundFX, duration + RandOffset);
    }
}

void ACombatant::ResetSoundFX()
{
    isPlayingSoundFX = false;
}

bool ACombatant::IsEnemyFor(AController* TestPC) const
{
    if (TestPC == Controller || TestPC == NULL)
    {
        return false;
    }
	AFan *otherFan = Cast<AFan>(TestPC->GetPawn());
	if (otherFan){
		return true;
	}

	ACombatant *otherCombatant = Cast<ACombatant>(TestPC->GetPawn());
    
    if(otherCombatant == nullptr)
        return false;
    
    bool bIsEnemy = true;
    if (otherCombatant->alliance != nullptr)
    {
        bIsEnemy = otherCombatant->alliance != alliance;
		/*if (otherCombatant->alliance == alliance){
			GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("Brother!"));
		} else {
			GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("Hater!"));
		}*/
    }
	//GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("Stranger!"));
    return bIsEnemy;
}

void ACombatant::OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser) {
	//if (alliance) GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("A member of the " + alliance->name() + " was killed."));
	//else GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("A rouge combatant was killed."));
	AShooterCharacter::OnDeath(KillingDamage, DamageEvent, InstigatingPawn, DamageCauser);
    
    ACombatant *killerCombatant = Cast<ACombatant>(InstigatingPawn);
    if (killerCombatant) {
        killerCombatant->OnKill(this);
		killedMe = killerCombatant;
    }
    this->PlaySoundFX(AUDIO_TYPES::DEATH_CRY);
}

void ACombatant::OnKill(ACombatant *KilledCombatant) {
    if (!KilledCombatant) return;
    numKillPoints += 1.0;
    //numJustActionPoints += 1.0 - KilledCombatant->GetJusticeScore();
	//numTotalActionPoints += 1.0;
	//OR...
	if (KilledCombatant->GetJusticeScore() < 0) numJustActionPoints -= KilledCombatant->GetJusticeScore()*(100.0 - abs(numJustActionPoints)) / 100.0;
	else numJustActionPoints -= (KilledCombatant->GetJusticeScore() + 1);
	if (numJustActionPoints < -100.0) numJustActionPoints = -100.0;
	if (numJustActionPoints > 100.0) numJustActionPoints = 100.0;
}

void ACombatant::Tick(float DeltaSeconds) {
    Super::Tick(DeltaSeconds);
    numKillPoints -= DeltaSeconds * KILL_POINTS_TICK_DROPOFF;
    
    if (numKillPoints < 0) {
        numKillPoints = 0.0;
    }
}

void ACombatant::BeginPlay() {
	AShooterCharacter::BeginPlay();
	if (alliance) JoinAlliance(alliance);
    
    isPlayingSoundFX = false;
    //const float MaxNumSecondsWait = 10.0;
    //float RandOffset =  (float)rand()/INT_MAX;
    //GetWorldTimerManager().SetTimer(this, &ACombatant::ResetSoundFX, RandOffset);
    RandomizePersonality();
}

void ACombatant::RandomizePersonality() {
    W_BLOODLUST = FMath::RandRange(0, 100);
    W_JUSTICE = FMath::RandRange(0, 100);
    W_INDEPENDENCE = FMath::RandRange(0, 100);
    W_LOYALTY = FMath::RandRange(0, 100);
    W_EGO = FMath::RandRange(0, 100);
    W_SURVIVAL = FMath::RandRange(0, 100);
}
